import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {LivePlayer} from "react-native-live-stream";

const Video2Screen = () =>  {
      return (
      <View>
        <Text> React Native Live Stream </Text>
        <LivePlayer source={{uri:"http://aljazeera-eng-hd-live.hls.adaptive.level3.net/aljazeera/english2/index.m3u8"}}
   ref={(ref) => {
       this.player = ref
   }}
   style={styles.video}
   paused={false}
   muted={false}
   bufferTime={300}
   maxBufferTime={1000}
   resizeMode={"contain"}
   onLoading={()=>{}}
   onLoad={()=>{}}
   onEnd={()=>{}}
/>
      </View>
    );
  };

const styles = StyleSheet.create({

});
export default Video2Screen;
