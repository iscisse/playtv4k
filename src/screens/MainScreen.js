import React from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

const MainScreen = props =>  {
      return (
      <View>
       
       <Button 
            onPress={() => props.navigation.navigate('Video2')}
            title="React Native Live Stream"/>

        <Button 
            onPress={() => props.navigation.navigate('Video1')}
            title="React Native Video"/>
       
       
      </View>
    );
  };

const styles = StyleSheet.create({

});
export default MainScreen;
