import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
//import Video from 'react-native-video';

const Video1Screen = () =>  {
      return (
      <View>
        <Text> React Native Video </Text>
        <Video source={{uri: "background"}}   // Can be a URL or a local file.
       ref={(ref) => {
         this.player = ref
       }}                                      // Store reference
       onBuffer={this.onBuffer}                // Callback when remote video is buffering
       onError={this.videoError}               // Callback when video cannot be loaded
       style={styles.backgroundVideo} />
 
      

      
      </View>
    );
  };

const styles = StyleSheet.create({

});
export default Video1Screen;
