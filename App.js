import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import MainScreen from './src/screens/MainScreen';
import Video1Screen from './src/screens/Video1Screen';
import Video2Screen  from './src/screens/Video2Screen';

const navigator =  createStackNavigator({
    Main: MainScreen,
    Video1: Video1Screen,
    Video2: Video2Screen
},{
  initialRouteName: 'Main',
  defaultNavigationOptions:{
    title: 'Main'
  }
});

export default createAppContainer(navigator);